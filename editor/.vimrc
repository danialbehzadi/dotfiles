"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax on
filetype plugin indent on

" Show line numbers
set number

" Sets how many lines of history VIM has to remember
set history=700

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Makes search act like search in modern browsers
set incsearch

" Show (patial) command in status line
set showcmd

" Show matching brackets
set showmatch

" Support BiDi
set termbidi

" Let buffers move without save
set hidden

" Integrate with system clipboard
set clipboard=unnamedplus


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Improvment for dark backgrounds
set background=dark
colo desert

" Enable syntax highlighting
syntax enable

" Set extra options when running in GUI mode
if has("gui_running")
    set guioptions-=T
    set guioptions+=e
    set t_Co=256
    set guitablabel=%M\ %t
endif

"" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ai "Auto indent
"set si "Smart indent


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Smart way to move between windows
noremap <C-j> <C-W>j
noremap <C-k> <C-W>k
noremap <C-h> <C-W>h
noremap <C-l> <C-W>l

" Firefox style buffer navigation
nnoremap <C-PageUp> :bp<CR>
inoremap <C-PageUp> <Esc>:bp<CR>i
nnoremap <C-PageDown> :bn<CR>
inoremap <C-PageDown> <Esc>:bn<CR>i
nnoremap <C-w> <Esc>:bd<CR>

" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Remember info about open buffers on close
set viminfo^=%


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
"set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

" Returns true if paste mode is enabled
"function! HasPaste()
"    if &paste
"        return 'PASTE MODE  '
"    en
"    return ''
"endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
"map <leader>ss :setlocal spell!<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Netrw
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keep the current directory and the browsing directory synced
" This helps you avoid the move files error
let g:netrw_keepdir = 0

" Hide the banner. To show it temporarily you can use I inside Netrw
let g:netrw_banner = 0

" Change the copy command. Mostly to enable recursive copy of directories
let g:netrw_localcopydircmd = 'cp -r'

" Highlight marked files in the same way search matches are
hi! link netrwMarkFile Search

" Keybinding for Netrw, but first set the split size to 25%
nnoremap <F12> :let g:netrw_winsize = 25<CR>:Lexplore<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Linting and fixing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ALE is an async linter for vim
" you should install `vim-ale`
" you might need the lsp package for the language as well
packadd! ale
" Ale's own auto complete
" Commented out because we use youcompleteme
"let g:ale_completion_enabled = 1
" Show problem previews on going to line
let g:ale_cursor_detail = 1
" Use ballon instead of window
let ale_floating_preview = 1
" Close ballon when going to insert mode
let g:ale_close_preview_on_insert = 1
" Minimum severity level of LSP message
let g:ale_lsp_show_message_severity = 'warning'
" Show hints/suggestions from LSP servers
let ale_lsp_suggestions = 1

" specify linters for languages
" you should have them installed
" For python we need `flake8 pylint python3-pylsp`
let g:ale_linters = {
	\'python': ['pylsp', 'pylint'],
	\}

" specify fixers for languages
" you should have them installed
let g:ale_fixers = {
	\'python': ['black', 'autoimport'],
	\'sh': ['shfmt'],
	\'xml': ['xmllint'],
	\'yaml': ['prettier', 'yamlfix'],
        \'*': ['remove_trailing_lines', 'trim_whitespace'],
	\}
" set ling lenght to 79 for black
let g:ale_python_black_options='--line-length=79'

" show warnings and errors count in statusline
" airline has a built-in support for this
"function! LinterStatus() abort
"  let l:counts = ale#statusline#Count(bufnr(''))
"  let l:all_errors = l:counts.error + l:counts.style_error
"  let l:all_non_errors = l:counts.total - l:all_errors
"  return l:counts.total == 0 ? '✨ all good ✨' : printf(
"        \   '😞 %dW %dE',
"        \   all_non_errors,
"        \   all_errors
"        \)
"endfunction
"set statusline=
"set statusline+=%m
"set statusline+=\ %f
"set statusline+=%=
"set statusline+=\ %{LinterStatus()}

"set a button for fix
nmap <F10> <Plug>(ale_fix)

"auto fix on saving files
"let g:ale_fix_on_save = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Language Specific
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !exists("autocommands_loaded")
    let autocommands_loaded = 1
    " Python
    autocmd BufRead,BufNewFile,FileReadPost *.py source ~/.vim/python.vim
endif
au BufNewFile *.py 0r ~/.vim/skeleton.py
