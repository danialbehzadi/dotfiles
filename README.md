# Managed dotfiles
This is the dotfiles repo managed by [GNU Stow](https://www.gnu.org/software/stow).

## Setup
To use these structure, create your own branch for local usage:

```sh
(main)$ git checkout -b local
```

First of all, remove any unmodifed dotfiles in your home directory which are present in this repo too (e.g. `.bashrc`). For duplicate files, stow will try to adopt your own modifications into the `local` branch. Symlink any section of the repo you want:
```sh
(local)$ stow --adopt --target=/home/$USER dev
(local)$ stow --adopt --target=/home/$USER shell
…
```

After each manual change in the dotfiles managed by stow, commit the changes into your `local` branch. Squashing your local git commits will save you a handful of time when updating.

List of files which manual change is mandatory for them:
```sh
dev/.config/bash/rc/credentials.sh # EMAIL, DEBEMAIL and DEBFULLNAME
dev/.gitconfig #  email, name and signingkey
dev/.mailrc #  smtp, smtp-auth-user and smtp-auth-password
editor/.vim/skeleton.py #  Copyright line
music/.config/mpdscribble/mpdscribble.conf #  username and password
```

## Update
To get updates, you would try to update the `main` branch, then `rebase` your `local` branch to it:
```sh
(local)$ git checkout main
(main)$ git pull
(main)$ git checkout local
(local)$ git rebase main
```
You may resolve the conflicts if there is any.

## Move
If you want to move your own local changes to another machine, you can make a patch from yourr `local` branch against `main`:
```sh
(local)$ git format-patch main
```

You can then setup the repo on the other machine and apply the patch into its `local` branch:
```sh
(local)$ git apply *.patch
```
