# Setup GUIX
export GUIX_PROFILE="/home/$USER/.config/guix/current"
source "$GUIX_PROFILE/etc/profile"
export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
export PATH=/home/$USER/.guix-profile/bin/:$PATH
